extends "res://Scripts/game_manager.gd"

var player1_node
var player2_node
var player_node

# Called when the node enters the scene tree for the first time.
func _ready():
	._init_game()
	player_node = null
	player1_node = $"../player1"
	player2_node = $"../player2"
	deck_position = Vector2(520, 380)

func update_score():
	player_node.update_score()
	if player_node == player1_node:
		return Vector2(70 , 100)
	return Vector2(380 , 950)

# add the card the player marked
# dim chosen cards and undim those are regret chosen or 3 cards (set or not)
func choose_card(card_num):
	if player_node == null:
		return
	if add_chosen_card(card_num) == globals.cards_in_set:
		var is_set = check_marked_cards()
		player_node.end_player_turn(not is_set)
		player_node = null

func _on_player1_pressed():
	if player_node != null:
		return
	player_node = player1_node
	player_node.start_player_turn()

func _on_player2_pressed():
	if player_node != null:
		return
	player_node = player2_node
	player_node.start_player_turn()

func end_turn(found_set = false):
	if not found_set:
		# release the chosen cards
		undim_cards()
		marked_set.clear()
	player_node = null

func _on_card0_pressed():
	choose_card(0)
func _on_card1_pressed():
	choose_card(1)
func _on_card2_pressed():
	choose_card(2)
func _on_card3_pressed():
	choose_card(3)
func _on_card4_pressed():
	choose_card(4)
func _on_card5_pressed():
	choose_card(5)
func _on_card6_pressed():
	choose_card(6)
func _on_card7_pressed():
	choose_card(7)
func _on_card8_pressed():
	choose_card(8)
func _on_card9_pressed():
	choose_card(9)
func _on_card10_pressed():
	choose_card(10)
func _on_card11_pressed():
	choose_card(11)
func _on_card12_pressed():
	choose_card(12)
func _on_card13_pressed():
	choose_card(13)
func _on_card14_pressed():
	choose_card(14)
func _on_card15_pressed():
	choose_card(15)
func _on_card16_pressed():
	choose_card(16)
func _on_card17_pressed():
	choose_card(17)
func _on_card18_pressed():
	choose_card(18)
func _on_card19_pressed():
	choose_card(19)
func _on_card20_pressed():
	choose_card(20)
