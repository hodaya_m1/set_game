extends Control

var score
var blocked
var player_turn

signal press_set
signal end_player_turn

func _ready():
	score = 0
	$block_player_timer.wait_time = globals.WRONG_SET_PENALTY
	$block_player_timer.one_shot = true
	player_turn = false

func init_player(color, set_image):
	$button/progress.set_tint_progress(color)
	$score.set("custom_colors/font_color",color)
	$set_logo.texture = set_image
	blocked = false

func update_score():
	score += 1
	$score.text = String(score)
	return $score.rect_position

func get_score():
	return score

func _on_button_pressed():
	if blocked:
		return
	emit_signal("press_set")

func start_player_turn():
	player_turn = true
	$button.start_set_progress()

func pause_player_turn(pause): # pause or resume
	$button.disabled = pause
	$button.pause_set_progress(pause)
	if blocked:
		$block_player_timer.set_paused(pause)

func reset_player_progress():
	$button.reset_progress()

func end_player_turn(block = true):
	blocked = block
	$button.reset_set_progress(not block)
	if block:
		$block_player_timer.start()
		emit_signal("end_player_turn")
	else:
		player_turn = false

func unblock_player():
	end_player_turn(false)
