extends Node2D

func _on_play_pressed():
	get_node("option_screen").move(Vector2(-576,0))
	get_node("game_mode").move(Vector2(213,0))

func _on_back_pressed():
	get_node("option_screen").move(Vector2(0,0))
	get_node("game_mode").move(Vector2(576,0))

func _on_how_to_play_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/instructions.tscn")

func _on_setting_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/settings.tscn")

func _on_score_table_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/score_table.tscn")
