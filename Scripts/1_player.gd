extends Node2D

var is_paused
var is_game_over

func _ready():
	connect_cards()
	is_paused = false
	is_game_over = false

func connect_cards():
	var grid = $board/cards_grid
	var game_manager = get_node("1_player_manager")
	var function_name = "_on_card%d_pressed" 
	for i in range(globals.max_full_board):
		var function = function_name % i
		grid.grid[i].connect("pressed", game_manager, function)

func pause_game():
	is_paused = true
	$bottom_ui.disable_buttons(true)
	$bottom_ui.pause_hint_progress(true)
	$pause_screen/HBoxContainer/continue.set_disabled(false)
	$top_ui/Timer.set_paused(true)
	$pause_screen.move(Vector2(57,130))

func resume_game():
	is_paused = false
	$bottom_ui/HBoxContainer/pause.set_disabled(false)
	$bottom_ui.hint_bottun_disabled(false)
	$bottom_ui.pause_hint_progress(false)
	$top_ui/Timer.set_paused(false)
	$pause_screen.move(Vector2(576,0))

func game_over():
	is_game_over = true
	$bottom_ui.disable_buttons(true)
	$top_ui/Timer.stop()
	$game_over.declare_game_over(game_over_text())

func game_over_text():
	var text = ""
	var data = $top_ui.get_player_data()
	var in_score_table = globals.save_score(data[1], data[2], data[0], globals.mode)
	if in_score_table:
		text = "You are one of the best!"
	else:
		text = "You did it!!"
	text += "\nyou found %d set" % data[0]
	if data[0] > 1:
		text += "s" 
	text += " in "
	if data[1] > 0:
		text += "%d minutes and " % data[1]
	if data[2] > 0:
		text += "%d seconds" % data[2]
	return text

func end_game():
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")
