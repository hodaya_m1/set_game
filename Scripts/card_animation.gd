extends Node2D

signal reveal_card

func move_to_board(card_texture, start, target, time):
	$Sprite.texture = card_texture
	$Tween.interpolate_property(self, "position", start, target, time, Tween.TRANS_BACK, Tween.EASE_IN)
	$Tween.interpolate_property(self, "scale", Vector2(0,0), Vector2(1,1), time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

func move_to_set(card_texture, start, target, time):
	$Sprite.texture = card_texture
	$Tween.interpolate_property(self, "position", start, target, time, Tween.TRANS_CIRC, Tween.EASE_IN)
	$Tween.interpolate_property(self, "scale", Vector2(1,1), Vector2(0,0), time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()

func _on_Tween_tween_all_completed():
	$Sprite.texture = null
	emit_signal("reveal_card")
