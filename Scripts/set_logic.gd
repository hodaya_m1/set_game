extends Node

const different_conditions = Vector3(1, 1, 1)
const same_condition1 = Vector3(3, 0, 0)
const same_condition2 = Vector3(0, 3, 0)
const same_condition3 = Vector3(0, 0, 3)

# check if the attributes of the marked set are the same of different in all of the cards.
# if they are all the same or all different - return true
# else - return false
func check_attribute(attributes):
	if attributes == different_conditions:
		return true
	else:
		if attributes[0] == 2 or attributes[1] == 2 or attributes[2] == 2:
			return false
	return true

func is_set(cards):
	assert(cards.size() == 3)
	var is_set
	var color = cards[0].color + cards[1].color + cards[2].color
	var shape = cards[0].shape + cards[1].shape + cards[2].shape
	var number = cards[0].number + cards[1].number + cards[2].number
	var shading = cards[0].shading + cards[1].shading + cards[2].shading
	var background = cards[0].background + cards[1].background + cards[2].background
	is_set = check_attribute(color) and check_attribute(shape) and check_attribute(number) and check_attribute(shading) and check_attribute(background)
	if is_set: # valid set
		return true
	return false
