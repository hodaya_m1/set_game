extends Node

const BOARD_HEIGHT = 1024
const BOARD_WIDTH = 576
const CARD_HEIGHT = 96
const CARD_WIDTH = 144

"""
explanation of teh different modes:
	BEGINNER = The deck consists of 27 cards that vary in three features across 
			   three possibilities for each kind of feature (number, shape, shading).
	REGULAR = The deck consists of 81 unique cards that vary in four features 
			  across three possibilities for each kind of feature (number, shape, shading, color)
	EXPERT = The deck consists of 256 unique cards that vary in five features 
			  across three possibilities for each kind of feature (number, color, shape, shading, background)
"""
enum GAME_MODE{BEGINNER = 0, REGULAR = 1, EXPERT = 2}
enum EXPERT_DECK_SIZE{HARD = 0, MEDIUM = 1, EASY = 2}

enum SOUND{FOUND_SET = 0, WRONG_SET = 1, CLICK = 2, OPEN_CARDS = 3, BACKGROUND = 4}

# general
const cards_in_line : int = 3
const cards_in_set : int = 3
const dim_card : float = 0.5
var min_full_board : int
var max_full_board : int
var total_cards : int
var remove_star = 0

# settings values
var mode = GAME_MODE.REGULAR
var music = true
var sound = true
var allow_hint = true
var size_of_deck_expert = EXPERT_DECK_SIZE.HARD

# 1 player constants
const HINT_PENALTY_SEC = 60

# 2 players constants
const player1_color = Color(0.92, 0.81, 0.44)
const player2_color = Color(0, 1, 0.5)
const TIME_SET_SELECTION = 4
const WRONG_SET_PENALTY = 3

# saving data
var file = File.new() 
const score_data_path = "user://score_data.save"
var score_data = [[],[],[]]
const settings_data_path = "user://settings_data.save"
var settings_data = [mode, music, sound, allow_hint, size_of_deck_expert]
const save_path_old_version = "user://savegame.save" # old version data
const highscore_capacity = 5
var mode_index = 0
var music_index = 1
var sound_index = 2
var hint_index = 3
var expert_deck_index = 4
func _ready():
	randomize()
	#var random_seed = randi()
	#seed(random_seed)
	#print("seed ", random_seed)
	if not file.file_exists(score_data_path): 
		create_file(score_data_path, score_data)
	else:
		score_data = read_file(score_data_path)
	if not file.file_exists(settings_data_path): 
		create_file(settings_data_path, settings_data)
	else:
		settings_data = read_file(settings_data_path)
		update_settings()
	if file.file_exists(save_path_old_version): # replace old version file if exist
		update_data_version()

func create_file(path, save_data):
	file.open(path, File.WRITE)
	file.store_var(save_data)
	file.close()

func update_settings():
	mode = settings_data[mode_index]
	music = settings_data[music_index]
	sound = settings_data[sound_index]
	allow_hint = settings_data[hint_index]
	if settings_data.size() > expert_deck_index:
		size_of_deck_expert = settings_data[expert_deck_index]
	update_game_mode()

func save_score(minutes, seconds, sets, save_mode):
	assert(sets > 0) # game end and save the score, when the last set found
	var in_highscore = true
	var time_in_sec = float(minutes * 60 + seconds)
	var avg_time = time_in_sec / sets
	var player_data = [minutes, seconds, sets, avg_time]
	score_data[save_mode].append(player_data)
	score_data[save_mode].sort_custom(self, "customComparison")
	if score_data[save_mode].size() > highscore_capacity:
		var remove_score = score_data[save_mode][highscore_capacity]
		score_data[save_mode].remove(highscore_capacity)
		if remove_score == player_data:
			in_highscore = false
	if in_highscore:
		store_data(score_data_path, score_data)
	return in_highscore

func customComparison(a, b):
	if a[0] != b[0]: # sort by minutes
		return a[0] < b[0]
	if a[1] != b[1]: # sort by seconds
		return a[1] < b[1]
	return a[3] <= b[3] # sort by number of sets

func read_file(path):
	file.open(path, File.READ) #open the file
	var data = file.get_var() #get the value
	file.close() #close the file
	return data #return the value

func get_score_data(score_mode):
	return score_data[score_mode]

func update_game_mode():
	if mode ==  GAME_MODE.BEGINNER:
		min_full_board = 6
		max_full_board = 12
		total_cards = 27
		remove_star = 10
	if mode ==  GAME_MODE.REGULAR:
		min_full_board = 12
		max_full_board = 21
		total_cards = 81
		remove_star = 20
	if mode ==  GAME_MODE.EXPERT:
		min_full_board = 15
		max_full_board = 15
		total_cards = 243
		remove_star = 30

func store_data(path, data):
	file.open(path, File.WRITE)
	file.store_var(data)
	file.close()

func save_settings():
	settings_data = [mode, music, sound, allow_hint, size_of_deck_expert]
	store_data(settings_data_path, settings_data)

func update_data_version():
	#read old data
	var data = read_file(save_path_old_version)["highscore"]
	score_data[GAME_MODE.REGULAR] = data # there is no previous data in score_data[GAME_MODE.REGULAR] 
	
	#delete old file
	var dir = Directory.new()
	dir.remove(save_path_old_version)
	
	#write the data in the new file
	store_data(score_data_path, score_data)
