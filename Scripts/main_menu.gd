extends Node2D

func _on_1_player_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/1_player_game.tscn")

func _on_2_players_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/2_players_game.tscn")

func _on_Quit_pressed():
	get_tree().quit()

func _notification(action):
	if action == MainLoop.NOTIFICATION_WM_QUIT_REQUEST: # For Windows
		get_tree().quit()
	if action == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: # For android
		get_tree().quit()
