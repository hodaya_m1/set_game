extends "res://Scripts/game_manager.gd"

signal update_score
var hide_cards

func _ready():
	._init_game()
	hide_cards = []
	get_parent().get_node("bottom_ui").hint_bottun_disabled(true)
	deck_position = Vector2(85, 85)

func update_score():
	emit_signal("update_score")
	return  Vector2(288, 65)

# add the card the player marked
# dim chosen cards and undim those are regret chosen or 3 cards (set or not)
func choose_card(card_num):
	if add_chosen_card(card_num) == globals.cards_in_set:
		if not hide_cards.empty():
			for card in hide_cards: # ignore the hint marks
				grid.reveal_card(card)
			hide_cards.clear()
		if check_marked_cards(): # founded set, allow ask for hint
			get_parent().get_node("bottom_ui").hint_bottun_disabled(false, true)
		if deck.size() == 0: # disable hints
			get_parent().get_node("bottom_ui").hint_bottun_disabled(true, true)

func _on_hint_request():
	if not hide_cards.empty():
		return
	get_parent().get_node("bottom_ui").hint_bottun_disabled(true, true)
	# delete chosen cards
	.undim_cards()
	marked_set.clear()
	hide_cards = get_hint(grid.num_open_cards)
	for card in hide_cards:
		grid.hide_card(card)
	get_parent().get_node("bottom_ui").start_hint_progress()

func get_hint(open_cards):
	var cards_in_grid = []
	var num_hide_cards = ceil(open_cards/3)
	for i in range(open_cards):
		cards_in_grid.append([i,0])
	for set in available_sets:
		for card in set:
			cards_in_grid[card][1] += 1
	cards_in_grid.sort_custom(self, "sort")
	for i in range(num_hide_cards):
		hide_cards.append(cards_in_grid[i][0])
	emit_signal("update_score", -1)
	return hide_cards

static func sort(a, b):
	if a[1] < b[1]:
		return true
	return false

func _on_card0_pressed():
	choose_card(0)
func _on_card1_pressed():
	choose_card(1)
func _on_card2_pressed():
	choose_card(2)
func _on_card3_pressed():
	choose_card(3)
func _on_card4_pressed():
	choose_card(4)
func _on_card5_pressed():
	choose_card(5)
func _on_card6_pressed():
	choose_card(6)
func _on_card7_pressed():
	choose_card(7)
func _on_card8_pressed():
	choose_card(8)
func _on_card9_pressed():
	choose_card(9)
func _on_card10_pressed():
	choose_card(10)
func _on_card11_pressed():
	choose_card(11)
func _on_card12_pressed():
	choose_card(12)
func _on_card13_pressed():
	choose_card(13)
func _on_card14_pressed():
	choose_card(14)
func _on_card15_pressed():
	choose_card(15)
func _on_card16_pressed():
	choose_card(16)
func _on_card17_pressed():
	choose_card(17)
func _on_card18_pressed():
	choose_card(18)
func _on_card19_pressed():
	choose_card(19)
func _on_card20_pressed():
	choose_card(20)
