extends Node2D

onready var cards_in_deck_label = $CenterContainer/container/remaining_cards
onready var deck_node = $CenterContainer/container/remaining_cards/cards
var update_deck = [57, 30, 12, 0]
var deck0 = load("res://Sprites/deck_0.png")
var deck1 = load("res://Sprites/deck_1.png")
var deck2 = load("res://Sprites/deck_2.png")
var deck3 = load("res://Sprites/deck_3.png")
var deck_im = [deck1, deck2, deck3, null]

signal pause_game

func update_deck_sprite():
	var deck_sprite
	deck_sprite = deck_im.pop_front()
	deck_node.set_texture(deck_sprite)

func update_cards_in_deck(remaining_cards = 0):
	if update_deck.size() and deck.size() <= update_deck[0]:
		update_deck.pop_front()
		update_deck_sprite()
	cards_in_deck_label.text = String(remaining_cards)

func _on_pause_pressed():
	emit_signal("pause_game")
