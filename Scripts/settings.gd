extends Node2D

func _ready():
	$VBoxContainer2/HBoxContainer/hint.pressed = globals.allow_hint
	$VBoxContainer2/sound_and_music/sound.pressed = globals.sound
	$VBoxContainer2/sound_and_music/music.pressed = globals.music
	$VBoxContainer/size_of_expert_deck.hide()
	if globals.mode == globals.GAME_MODE.BEGINNER:
		$VBoxContainer/CenterContainer/game_mode/beginner.pressed = true
		$VBoxContainer2/HBoxContainer/hint.disabled = true
	elif globals.mode == globals.GAME_MODE.REGULAR:
		$VBoxContainer/CenterContainer/game_mode/regular.pressed = true
	else:
		$VBoxContainer/CenterContainer/game_mode/expert.pressed = true
		load_expert_size()

func _on_back_button_pressed():
	globals.save_settings()
	globals.update_game_mode()
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")

func _on_hint_toggled(button_pressed):
	globals.allow_hint = button_pressed

func _on_sound_toggled(button_pressed):
	globals.sound = button_pressed

func _on_music_toggled(button_pressed):
	globals.music = button_pressed

func _on_beginner_pressed():
	globals.mode = globals.GAME_MODE.BEGINNER
	globals.allow_hint = false
	$VBoxContainer2/HBoxContainer/hint.disabled = true
	$VBoxContainer/size_of_expert_deck.hide()

func _on_regular_pressed():
	globals.mode = globals.GAME_MODE.REGULAR
	$VBoxContainer2/HBoxContainer/hint.disabled = false
	$VBoxContainer/size_of_expert_deck.hide()

func _on_expert_pressed():
	globals.mode = globals.GAME_MODE.EXPERT
	$VBoxContainer2/HBoxContainer/hint.disabled = false
	load_expert_size()

func load_expert_size():
	$VBoxContainer/size_of_expert_deck.show()
	match globals.size_of_deck_expert:
		globals.EXPERT_DECK_SIZE.HARD:
			$VBoxContainer/size_of_expert_deck/full.pressed = true
		globals.EXPERT_DECK_SIZE.MEDIUM:
			$VBoxContainer/size_of_expert_deck/two_thirds.pressed = true
		globals.EXPERT_DECK_SIZE.EASY:
			$VBoxContainer/size_of_expert_deck/third.pressed = true

func _on_full_pressed():
	globals.size_of_deck_expert = globals.EXPERT_DECK_SIZE.HARD

func _on_two_thirds_pressed():
	globals.size_of_deck_expert = globals.EXPERT_DECK_SIZE.MEDIUM

func _on_third_pressed():
	globals.size_of_deck_expert = globals.EXPERT_DECK_SIZE.EASY

func _notification(action):
	if action == MainLoop.NOTIFICATION_WM_QUIT_REQUEST: # For Windows
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")   
	if action == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: # For android
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")
