extends Panel

func _on_Quit_button_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")

func _on_Restart_button_pressed():
	var _ignore = get_tree().reload_current_scene()

func move(target):
	var move_tween = get_node("move_tween")
	move_tween.interpolate_property(self, "rect_position", self.rect_position, target, 2, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	move_tween.start()

func declare_game_over(text):
	$MarginContainer/data_on_game.text = String(text)
	var move_tween = get_node("move_tween")
	move_tween.interpolate_property(self, "rect_position", self.rect_position, Vector2(57, 130), 2, Tween.TRANS_QUINT, Tween.EASE_OUT)
	move_tween.start()
