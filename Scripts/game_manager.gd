extends Node

onready var grid

var start_game_timer
export (Vector2) var deck_position

var available_sets = [] # all the sets on the board
var marked_set = [] # position of cards the player marked
var animation_cards = []

# data for expert mode.
var expert_mode

# signal for updating the deck
signal update_cards_in_deck

const card_animation = preload("res://Scenes/card_animation.tscn")

func _init_game():
	var card
	grid= $"../board/cards_grid"
	# first cards (size of the board) are for animation to board
	# last cards_in_set cards are for animation to set
	for i in range(globals.max_full_board + globals.cards_in_set):
		card = card_animation.instance()
		call_deferred("add_child",card)
		animation_cards.append(card)
		card.connect("reveal_card", grid , "reveal_card_in_grid", [i]) 
	globals.update_game_mode()
	expert_mode = (globals.mode == globals.GAME_MODE.EXPERT)
	deck.load_deck()
	grid.load_grid()
	emit_signal("update_cards_in_deck", deck.size())
	SoundManager.play(globals.SOUND.BACKGROUND)
	start_game_timer = Timer.new()
	start_game_timer.connect("timeout",self,"_on_timer_timeout") 
	#timeout is what says in docs, in signals
	#self is who respond to the callback
	#_on_timer_timeout is the callback, can have any name
	add_child(start_game_timer) #to process
	start_game_timer.start() #to start
	start_game_timer.set_wait_time(0.01)
   
func _on_timer_timeout():
	spawn_cards()
	remove_child(start_game_timer)

# open cards on the board
func spawn_cards():
	grid.add_cards(globals.min_full_board)
	var add_new_cards = []
	for i in range(globals.min_full_board):
		add_new_cards.append(i)
	animation_card_to_board(add_new_cards)
	check_legal_board()
	available_sets = []
	

func check_legal_board():
	var sets = count_sets()
	emit_signal("update_cards_in_deck", deck.size())
	if sets == 0 and deck.size() == 0:
		grid.end_game_declaration()
		get_parent().game_over()
		return
	var check_in_deck = int(deck.size() / globals.cards_in_set)
	var num_of_checks = 0
	while sets == 0 and num_of_checks <= check_in_deck:
		num_of_checks += 1
		if deck.size() == 0:
			grid.end_game_declaration()
			get_parent().game_over()
			return
		if expert_mode:
			expert_mode_handle()
			animation_card_to_board(marked_set)
		else:
			grid.add_cards(globals.cards_in_line)
			#if not marked_set.empty(): #i.e. not the beggining of the game
			animation_card_to_board([grid.num_open_cards - 3, grid.num_open_cards - 2, grid.num_open_cards - 1])
		emit_signal("update_cards_in_deck", deck.size())
		sets = count_sets()
	if expert_mode and sets == 0 and num_of_checks == check_in_deck: # the grid does not contain any set
		try_to_shuffle()

func animation_card_to_set(over_cards, node_pos):
	var i = 0
	var cards = grid.position_to_cards(over_cards)
	for num in over_cards:
		if cards[i] == null:
			break
		var pos = grid.grid_to_world(num)
		animation_cards[-(i+1)].move_to_set(cards[i].get_texture(), pos, node_pos, 1)
		i += 1

func animation_card_to_board(over_cards):
	SoundManager.play(globals.SOUND.OPEN_CARDS)
	var i = 0
	var cards = grid.position_to_cards(over_cards)
	for num in over_cards:
		if cards[i] == null:
			break
		var pos = grid.grid_to_world(num)
		animation_cards[over_cards[i]].move_to_board(cards[i].get_texture(), deck_position, pos, 1)
		i += 1

# in expert mode, there will be 21 cards and always have at least 1 set
# this function calls when there is no set on the board, we need to replace the 
# latest open cards with new cards
func expert_mode_handle():
	var start = false
	if marked_set.empty(): # when just open the cards
		marked_set = [0 ,1 ,2]
		start = true
	# push the cards to the bottom of the deck
	for card in grid.position_to_cards(marked_set):
		deck.back_to_deck(card)
	# reveal new cards
	grid.remove_cards(marked_set)
	if start:
		marked_set.clear()

func try_to_shuffle():
	# add the cards to the deck
	for card in grid.num_open_cards:
		deck.back_to_deck(grid.grid[card].card_node)
	# check if there is a set:
	# if there is no set - end the game
	var set_option = deck.check_if_exist_set()
	if set_option.empty():
		grid.end_game_declaration()
		get_parent().game_over()
		return
	# if there is a set - shuffle the deck and open cards
	for _i in range(min(globals.min_full_board - globals.cards_in_set, deck.size())):
		set_option.push_back(deck.next_card())
	set_option.shuffle()
	for i in range(set_option.size()):
		 grid.add_card_to_grid(set_option[i], i)
	grid.num_open_cards = set_option.size()

func update_available_sets():
	var delete_set = []
	var in_set
	for card in marked_set:
		for set in available_sets:
			in_set = set.find(card)
			if in_set != -1: #in case that other set include the card
				delete_set.append(set)
			if set.max() >= (grid.num_open_cards - globals.cards_in_set): # in case we reduce cards
				delete_set.append(set)
	for set in delete_set:
		available_sets.erase(set)

func count_sets():
	var cards_in_grid = grid.num_open_cards
	assert(cards_in_grid % globals.cards_in_set == 0 and cards_in_grid <= globals.max_full_board)
	var check_set = []
	for i in range (cards_in_grid - 2):
		check_set.append(i)
		for j in range (i + 1, cards_in_grid - 1):
			check_set.append(j)
			for k in range (j + 1, cards_in_grid):
				check_set.append(k)
				if available_sets.find(check_set) != -1:
					check_set.remove(2)
					continue
				if rules.is_set(grid.position_to_cards(check_set)):
					available_sets.append(check_set.duplicate())
				check_set.remove(2)
			check_set.remove(1)
		check_set.remove(0)
	#print_sets()
	return available_sets.size()

func print_sets():
	for set in available_sets:
		print(set)
	print("==============")

func check_marked_cards():
	undim_cards()
	var is_set = rules.is_set(grid.position_to_cards(marked_set))
	if is_set:
		SoundManager.play(globals.SOUND.FOUND_SET)
		var node_pos = update_score()
		animation_card_to_set(marked_set, node_pos)
		if grid.remove_cards(marked_set) != 0: # reduced the size of the grid
			available_sets.clear()
		else:
			animation_card_to_board(marked_set)
			update_available_sets()
		check_legal_board()
	else:
		SoundManager.play(globals.SOUND.WRONG_SET)
	clear_chosen_cards()
	return true if is_set else false

# add the card the player marked
# dim chosen cards and undim those are regret chosen or 3 cards (set or not)
func add_chosen_card(card_num):
	SoundManager.play(globals.SOUND.CLICK)
	var position = marked_set.find(card_num)
	if position == -1: # not already chosen
		marked_set.append(card_num)
		grid.select_card(card_num)
	else:
		marked_set.erase(card_num)
		grid.deselect_card(card_num)
	return marked_set.size()

func undim_cards():
	for pos in marked_set:
		grid.deselect_card(pos)

func clear_chosen_cards():
	marked_set.clear()

func _notification(action):
	if action == MainLoop.NOTIFICATION_WM_QUIT_REQUEST: # For Windows
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")   
	if action == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: # For android
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")

func _exit_tree():
	SoundManager.stop()

# DO NOT DELETE!!
func update_score(): # inherited children will implement
	pass

# DO NOT DELETE!!
func game_over_text(): # inherited children will implement
	pass
