extends TextureButton

var card_node
var in_set_progress

func _ready():
	card_node = null
	in_set_progress = false

func add_card(card):
	set_normal_texture(card.get_texture())
	card_node = card

func remove_card():
	set_normal_texture(null)
	card_node = null

func get_card():
	return card_node

func change_card_alpha(alpha):
	modulate = Color(1, 1, 1, alpha)

func select_card():
	#change_card_alpha(globals.dim_card)
	rect_scale = Vector2(1.1, 1.1)

func deselect_card():
	#change_card_alpha(1)
	rect_scale = Vector2(1, 1)

func remove_set():
	in_set_progress = true

func reveal_card():
	change_card_alpha(1)
	disabled = false

func hide_card():
	change_card_alpha(0)
	disabled = true

