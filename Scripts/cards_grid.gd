extends GridContainer

var grid = []

var num_open_cards = 0
var card_in_grid = preload("res://Scenes/card_in_board.tscn")

# add the places of the cards to the grid 
func load_grid():
	for _i in range(globals.max_full_board):
		var card_instance = card_in_grid.instance()
		grid.append(card_instance)
		add_child(card_instance)

func remove_card_from_grid(pos):
	assert(pos >= 0)
	grid[pos].remove_card()

func add_card_to_grid(card, pos, replace = true):
	assert(pos >= 0)
	grid[pos].add_card(card)
	if not replace:
		grid[pos].hide_card()

func reveal_card_in_grid(pos):
	assert(pos >= 0)
	if pos < num_open_cards:
		grid[pos].reveal_card()

# open new ^amount^ cards from the position
func add_cards(amount):
	if deck.size() == 0:
		#end_game_declaration()
		return
	for _i in range (amount):
		var card = deck.next_card()
		add_card_to_grid(card, num_open_cards, false)
		num_open_cards += 1

func position_to_cards(pos_array):
	var cards = []
	for pos in pos_array:
		cards.append(grid[pos].get_card())
	return cards

func replace_cards(marked_set):
	var remove_card = num_open_cards - 1
	marked_set.sort()
	for pos in range(2, -1, -1):
		var card_pos = marked_set[pos]
		add_card_to_grid(grid[remove_card].get_card(), card_pos)
		remove_card_from_grid(remove_card)
		num_open_cards -= 1
		remove_card -= 1

func reduce_cards(marked_set):
	marked_set.sort()
	var open_cards = []
	for i in range(num_open_cards):
		open_cards.append(grid[i].get_card())
	for i in range(2, -1, -1):
		open_cards.remove(marked_set[i])
		num_open_cards -= 1
	marked_set.clear()
	var replace_card = 0
	for card in open_cards:
		add_card_to_grid(card, replace_card)
		replace_card += 1
	for i in range(3):
		remove_card_from_grid(num_open_cards + i)

func remove_cards(marked_set):
	if num_open_cards > globals.min_full_board:
		replace_cards(marked_set)
		return -1
	if deck.size() == 0:
		reduce_cards(marked_set)
		return 
	for pos in marked_set:
		# create instance of that card
		add_card_to_grid(deck.next_card(), pos, false)
	return 0

func select_card(pos):
	grid[pos].select_card()

func deselect_card(pos):
	grid[pos].deselect_card()

func reveal_card(pos):
	grid[pos].reveal_card()

func hide_card(pos):
	grid[pos].hide_card()

func end_game_declaration():
	grid.clear()

func grid_to_world(pos):
	var num_of_cols = globals.cards_in_line 
	var num_of_rows = num_open_cards / num_of_cols
	var card_col = pos % num_of_cols
	var card_row = int(pos / num_of_cols)
	var h_sep = get("custom_constants/hseparation")
	var v_sep = get("custom_constants/vseparation")
	var grid_width = (num_of_cols * globals.CARD_WIDTH) + (num_of_cols - 1) * h_sep
	var grid_height = (num_of_rows * globals.CARD_HEIGHT) + (num_of_rows - 1) * v_sep
	var offset_X = int(globals.BOARD_WIDTH / 2.0 - grid_width / 2 + globals.CARD_WIDTH / 2.0)
	var offset_Y = int(globals.BOARD_HEIGHT / 2.0 - grid_height / 2)
	var card_pos_x = (card_col * globals.CARD_WIDTH) + (card_col) * h_sep
	var card_pos_y = (card_row * globals.CARD_HEIGHT) + (card_row) * v_sep
	return Vector2(card_pos_x + offset_X, card_pos_y + offset_Y)
