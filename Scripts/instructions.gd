extends Node2D

func _on_back_button_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")

func _notification(action):
	if action == MainLoop.NOTIFICATION_WM_QUIT_REQUEST: # For Windows
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")   
	if action == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: # For android
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")
