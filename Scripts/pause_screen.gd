extends Node

signal unpause_game

func move(target):
	var move_tween = get_node("move_tween")
	move_tween.interpolate_property(self, "rect_position", self.rect_position, target, 1.5, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
	move_tween.start()

func _on_continue_game_pressed():
	$HBoxContainer/continue.set_disabled(true)
	emit_signal("unpause_game")

func _on_quit_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")
