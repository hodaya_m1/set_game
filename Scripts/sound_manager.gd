extends Node

# sound
var found_set_sound
var wrong_set_sound
var click
var open_cards

var background_music

# Called when the node enters the scene tree for the first time.
func _ready():
	set_music()

func set_music():
	found_set_sound = AudioStreamPlayer.new()
	add_child(found_set_sound)
	found_set_sound.stream = load("res://Sound/set_found.wav")
	wrong_set_sound = AudioStreamPlayer.new()
	add_child(wrong_set_sound)
	wrong_set_sound.stream = load("res://Sound/wrong_set.wav")
	click = AudioStreamPlayer.new()
	add_child(click)
	click.stream = load("res://Sound/click.wav")
	open_cards = AudioStreamPlayer.new()
	add_child(open_cards)
	open_cards.stream = load("res://Sound/3_cards.wav")
	background_music = AudioStreamPlayer.new()
	add_child(background_music)
	background_music.stream = load("res://Sound/background_music.ogg")

func play(sound):
	if globals.sound:
		match sound:
			globals.SOUND.CLICK:
				click.play()
			globals.SOUND.FOUND_SET:
				found_set_sound.play()
			globals.SOUND.WRONG_SET:
				wrong_set_sound.play()
			globals.SOUND.OPEN_CARDS:
				open_cards.play()
	if globals.music:
		match sound:
			globals.SOUND.BACKGROUND:
				background_music.play()

func stop():
	found_set_sound.stop()
	wrong_set_sound.stop()
	click.stop()
	open_cards.stop()
	background_music.stop()
