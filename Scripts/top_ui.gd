extends TextureRect

# score
onready var score_label = $MarginContainer/HBoxContainer/number_of_sets
var current_score = 0

# time
onready var elapsed_time_label = $MarginContainer/HBoxContainer/elapsed_time
var seconds = 0
var minutes = 0

# deck animation
onready var cards_in_deck_label = $MarginContainer/HBoxContainer/remaining_cards
onready var deck_node = $MarginContainer/HBoxContainer/remaining_cards/cards
var update_deck
var deck0 = load("res://Sprites/deck_0.png")
var deck1 = load("res://Sprites/deck_1.png")
var deck2 = load("res://Sprites/deck_2.png")
var deck3 = load("res://Sprites/deck_3.png")
var deck_im = [deck0, deck1, deck2, deck3, null]

# starts snimation
var empty_star = load("res://Sprites/empty_star.png")
var full_star = load("res://Sprites/full_star.png")
onready var star1 = $MarginContainer/HBoxContainer/number_of_sets/star1
onready var star2 = $MarginContainer/HBoxContainer/number_of_sets/star2
onready var star3 = $MarginContainer/HBoxContainer/number_of_sets/star3
var all_stars
var append_star
var remove_star
var time_to_find_set
var last_set

func _ready():
	var deck_size = globals.total_cards
	update_deck = [int(0.7 * deck_size), int(0.35 * deck_size), int(0.17 * deck_size), 0]
	update_deck_sprite()
	all_stars = [star1, star2, star3]
	append_star = 3
	remove_star = 2
	for i in range(3):
		all_stars[i].set_texture(full_star)
	time_to_find_set = globals.remove_star
	last_set = time_to_find_set

func update_deck_sprite():
	var deck_sprite
	deck_sprite = deck_im.pop_front()
	deck_node.set_texture(deck_sprite)

func update_cards_in_deck(remaining_cards = 0):
	while update_deck.size() and remaining_cards <= update_deck[0]:
		update_deck.pop_front()
		update_deck_sprite()
	cards_in_deck_label.text = String(remaining_cards)

func update_stars_sprite(add_star = false):
	last_set = time_to_find_set
	if add_star and append_star == 3:
		return
	if !add_star and remove_star == -1:
		return
	var next_star = 1 if add_star else -1
	if add_star:
		all_stars[append_star].set_texture(full_star)
	else:
		all_stars[remove_star].set_texture(empty_star)
	append_star += next_star
	remove_star += next_star

func update_score(amount = 1):
	current_score += amount
	assert(current_score >= 0)
	if amount > 0:
		update_stars_sprite(true)
	score_label.text = String(current_score)

func _on_Timer_timeout():
	seconds += 1
	if seconds == 60:
		seconds = 0
		minutes += 1 
	var str_elapsed = "%02d : %02d" % [minutes, seconds]
	elapsed_time_label.text = String(str_elapsed)
	if last_set == 0:
		update_stars_sprite()
	last_set-=1

# return:
# 1. number of founded sets
# 2. elapsed minutes
# 3. elapsed seconds
func get_player_data():
	var data = []
	data.push_back(current_score)
	data.push_back(minutes)
	data.push_back(seconds)
	return data
