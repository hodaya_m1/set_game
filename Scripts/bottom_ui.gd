extends TextureRect

signal pause_game
signal hint_request

var pause_progress_bar
var in_progress
const FULL_BAR_VALUE = 360
const EMPTY_BAR_VALUE = 0
const BAR_SPEED = FULL_BAR_VALUE / globals.HINT_PENALTY_SEC
var current_bar_value

func _ready():
	if not globals.allow_hint:
		$HBoxContainer/hint.hide()
	$HBoxContainer/hint/time_progress.max_value = FULL_BAR_VALUE
	$HBoxContainer/hint/time_progress.step = 1
	pause_progress_bar = true
	in_progress = false
	hint_bottun_disabled(true, true)

func _on_pause_pressed():
	emit_signal("pause_game")

func _on_hint_pressed():
	if not in_progress:
		emit_signal("hint_request")

func start_hint_progress():
	pause_progress_bar = false
	in_progress = true 
	current_bar_value = EMPTY_BAR_VALUE

func pause_hint_progress(stop):
	pause_progress_bar = stop

func _process(delta):
	if pause_progress_bar or (not in_progress) or get_parent().is_game_over:
		return
	if get_parent().get_node("1_player_manager").grid.num_open_cards < globals.min_full_board:
		hint_bottun_disabled(true, true)
		return
	current_bar_value += BAR_SPEED * delta
	current_bar_value = min(current_bar_value, FULL_BAR_VALUE)
	if current_bar_value == FULL_BAR_VALUE:
		in_progress = false
		var score = get_parent().get_node("top_ui").current_score
		if score == 0:
			hint_bottun_disabled(true, true)
			return
		else:
			hint_bottun_disabled(false)
	$HBoxContainer/hint/time_progress.value = current_bar_value

func hint_bottun_disabled(disable, reset_progress = false):
	$HBoxContainer/hint.disabled = disable
	if reset_progress:
		$HBoxContainer/hint/time_progress.value = EMPTY_BAR_VALUE if disable else FULL_BAR_VALUE

func disable_buttons(disable):
	$HBoxContainer/pause.set_disabled(disable)
	$HBoxContainer/hint.set_disabled(disable)
