extends Node2D

var minutes = 0
var seconds = 1
var sets = 2
var avg_time = 3
const time_text_data = "%dm %ds"
const score_text_data = "%d sets\n%.1f seconds per set\nin average"

func _ready():
	load_mode_score(globals.GAME_MODE.BEGINNER, get_node("TabContainer/beginner"))
	load_mode_score(globals.GAME_MODE.REGULAR, get_node("TabContainer/regular"))
	load_mode_score(globals.GAME_MODE.EXPERT, get_node("TabContainer/expert"))
	$TabContainer.current_tab = globals.mode

func load_mode_score(mode, mode_tab):
	var time1 = mode_tab.get_node("time/time1")
	var time2 = mode_tab.get_node("time/time2")
	var time3 = mode_tab.get_node("time/time3")
	var time4 = mode_tab.get_node("time/time4")
	var time5 = mode_tab.get_node("time/time5")
	var time = [time1, time2, time3, time4, time5]
	var data1 = mode_tab.get_node("data/data1")
	var data2 = mode_tab.get_node("data/data2")
	var data3 = mode_tab.get_node("data/data3")
	var data4 = mode_tab.get_node("data/data4")
	var data5 = mode_tab.get_node("data/data5")
	var data = [data1, data2, data3, data4, data5]
	var score_data = globals.get_score_data(mode)
	for i in range(score_data.size()):
		var player_data = score_data[i]
		time[i].text = String(time_text_data % [player_data[minutes], player_data[seconds]])
		if player_data.size() == 4: # new version of score
			data[i].text = String(score_text_data % [player_data[sets], player_data[avg_time]])

func _on_back_button_pressed():
	var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")

func _notification(action):
	if action == MainLoop.NOTIFICATION_WM_QUIT_REQUEST: # For Windows
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")   
	if action == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST: # For android
		var _ignore = get_tree().change_scene("res://Scenes/main_menu.tscn")
