extends Node2D

onready var player1 = $player1
onready var player2 = $player2

# Called when the node enters the scene tree for the first time.
func _ready():
	$board.set_size(Vector2(476,1024))
	$board/cards_grid.set("custom_constants/hseparation", 11)
	$board/cards_grid.set("custom_constants/vseparation", 11)
	set_players_theme()
	connect_cards()

func set_players_theme():
	var set_texture = load("res://Sprites/set_player1.png")
	player1.init_player(globals.player1_color, set_texture)
	set_texture = load("res://Sprites/set_player2.png")
	player2.init_player(globals.player2_color, set_texture)

func connect_cards():
	var grid = $board/cards_grid
	var game_manager = get_node("2_players_manager")
	var function_name = "_on_card%d_pressed" 
	for i in range(globals.max_full_board):
		var function = function_name % i
		grid.grid[i].connect("pressed", game_manager, function)

func pause_game():
	$side_ui/CenterContainer/container/pause.set_disabled(true)
	$pause_screen/HBoxContainer/continue.set_disabled(false)
	player1.pause_player_turn(true)
	player2.pause_player_turn(true)
	$pause_screen.move(Vector2(5,130))

func resume_game():
	$side_ui/CenterContainer/container/pause.set_disabled(false)
	#var player = get_node("2_players_manager").player_node
	player1.pause_player_turn(false)
	player2.pause_player_turn(false)
	$pause_screen.move(Vector2(576,0))

func game_over():
	$side_ui/CenterContainer/container/pause.set_disabled(true)
	$player1/button.pause_set_progress(true)
	$player2/button.pause_set_progress(true)
	$game_over.declare_game_over(game_over_text())

func game_over_text():
	var player1_score = $player1.get_score()
	var player2_score = $player2.get_score()
	var text = "player1: %d \nplayer2: %d\n" % [player1_score, player2_score]
	if player1_score > player2_score:
		text += "player1 WON!!"
	elif player1_score < player2_score:
		text += "player2 WON!!"
	else:
		text += "it is a tie"
	return text
