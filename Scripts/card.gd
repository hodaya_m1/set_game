extends TextureRect

export (Vector3) var color # blue, green, purple, red
export (Vector3) var shape # oval, diamond, squiggle
export (Vector3) var number # 1 2 3
export (Vector3) var shading # open, stripes, solid 
export (Vector3) var background
var card_node

func print():
	print("color: ", color, ", shape: ", shape, ", number: ", number, ", shading: ", shading)
