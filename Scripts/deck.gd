extends Node

const all_possible_sets : int = 1080
var full_deck = []
var deck = []

var colors = {1: 'b', 2: 'g', 3: 'p'}
var numbers = {1: '1', 2: '2', 3: '3'}
var shapes = {1: 'o', 2: 'd', 3: 's'}
var shades = {1: 'o', 2: 's', 3: 'f'}
var backgrounds = {1: 'w', 2: 'g', 3: 'b'}

var attribute_value = {1: Vector3(1, 0, 0), 2: Vector3(0,1,0), 3: Vector3(0,0,1)}
const card = preload("res://Scenes/card.tscn")

func _init():
	#var random_seed = randi()
	#seed(random_seed)
	#print("seed ", random_seed)
	load_cards()

func load_deck():
	deck = []
	update_deck(globals.mode)

# load all cards and their attributes
func load_cards():
	for color in colors.keys():
		for num in numbers.keys():
			for shape in shapes.keys():
				for shade in shades.keys():
					for bg in backgrounds.keys():
						var card_instance = card.instance()
						var image = colors[color] + numbers[num] + shapes[shape] + shades[shade] + backgrounds[bg]
						var texture = load("res://Sprites/cards/" + image + ".png")
						card_instance.set_texture(texture)
						card_instance.color = attribute_value[color]
						card_instance.shape = attribute_value[shape]
						card_instance.number = attribute_value[num]
						card_instance.shading = attribute_value[shade]
						card_instance.background = attribute_value[bg]
						full_deck.append(card_instance)

# return the next in the deck
func next_card():
	return deck.pop_front()

func back_to_deck(card):
	deck.push_back(card)

func size():
	return deck.size()

func check_if_exist_set():
	var cards_in_grid = deck.size()
	assert(cards_in_grid % globals.cards_in_set == 0)
	var check_cards = []
	for i in range (cards_in_grid - 2):
		check_cards.append(deck[i])
		for j in range (i + 1, cards_in_grid - 1):
			check_cards.append(deck[j])
			for k in range (j + 1, cards_in_grid):
				check_cards.append(deck[k])
				if rules.is_set(check_cards):
					deck.remove(k)
					deck.remove(j)
					deck.remove(i)
					return check_cards
				check_cards.remove(2)
			check_cards.remove(1)
		check_cards.remove(0)
	return check_cards

func update_beginner_deck():
	"""
	The deck consists of 27 cards that vary in three features across three 
	possibilities for each kind of feature (color, number, shape).
	"""
	deck = []
	var num_card = 6
	for color in [1, 2, 3]:
		for num in [1, 2, 3]:
			for shape in [1, 2, 3]:
				deck.append(full_deck[num_card])
				num_card += 9
	deck.shuffle()

func update_regular_deck():
	"""
	The deck consists of 81 unique cards that vary in four features across three
	possibilities for each kind of feature (number, shape, shading, color)
	"""
	deck = []
	var num_card = 0
	for color in [1, 2, 3]:
		for num in [1, 2, 3]:
			for shape in [1, 2, 3]:
				for shade in [1, 2, 3]:
					deck.append(full_deck[num_card])
					num_card += 3  # skip backgroung
	deck.shuffle()

func update_expert_deck():
	"""
	The deck consists of 256 unique cards that vary in four features across
	four possibilities for each kind of feature (number, color, shape, shading)
	"""
	deck = []
	var num_card = 0
	for color in [1, 2, 3]:
		for num in [1, 2, 3]:
			for shape in [1, 2, 3]:
				for shade in [1, 2, 3]:
					for bg in [1, 2, 3]:
						deck.append(full_deck[num_card])
						num_card += 1
	deck.shuffle()
	var remove_cards = (globals.size_of_deck_expert * globals.total_cards) / 3
	for _i in remove_cards:
		deck.pop_back()

func update_deck(game_mode):
	if game_mode == globals.GAME_MODE.BEGINNER:
		update_beginner_deck()
	if game_mode == globals.GAME_MODE.REGULAR:
		update_regular_deck()
	if game_mode == globals.GAME_MODE.EXPERT:
		update_expert_deck()

func _exit_tree():
	for card in full_deck:
		card.free()

"""
func test_all_sets():
	var count_sets = 0
	var cards_in_deck =  81
	var is_set
	for i in range (cards_in_deck - 2):
		for j in range (i + 1, cards_in_deck - 1):
			for k in range (j + 1, cards_in_deck):
				var color = deck[i].color + deck[j].color + deck[k].color
				var shape = deck[i].shape + deck[j].shape + deck[k].shape
				var number = deck[i].number + deck[j].number + deck[k].number
				var shading = deck[i].shading + deck[j].shading + deck[k].shading
				is_set = check_attribute(color) and check_attribute(shape) and check_attribute(number) and check_attribute(shading)
				if is_set: # valid set
					count_sets += 1
	assert(count_sets == all_possible_sets)
"""
