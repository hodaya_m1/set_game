extends Button

var pause_progress_bar
var in_progress
const FULL_BAR_VALUE = 360
const EMPTY_BAR_VALUE = 0
const BAR_SPEED = FULL_BAR_VALUE / globals.TIME_SET_SELECTION
var current_bar_value

signal end_turn

func _ready():
	$progress.max_value = FULL_BAR_VALUE
	$progress.step = 1
	pause_progress_bar = false
	in_progress = false
	current_bar_value = FULL_BAR_VALUE
	reset_buttun_progress(current_bar_value)

func start_set_progress():
	in_progress = true
	current_bar_value = EMPTY_BAR_VALUE

func pause_set_progress(pause):
	pause_progress_bar = pause

func reset_set_progress(full = false):
	in_progress = false
	reset_buttun_progress(FULL_BAR_VALUE if full else EMPTY_BAR_VALUE)

func _process(delta):
	if pause_progress_bar or not in_progress:
		return
	current_bar_value += BAR_SPEED * delta
	current_bar_value = min(current_bar_value, FULL_BAR_VALUE)
	$progress.value = current_bar_value
	if current_bar_value == FULL_BAR_VALUE:
		emit_signal("end_turn")
		in_progress = false

func reset_buttun_progress(bar_value):
	$progress.value = bar_value
